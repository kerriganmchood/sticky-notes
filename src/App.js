import React from 'react';
import Stickynotes from './Stickynotes';

// Main
function App() {
	return (
		<Stickynotes />
	);
}

export default App;
