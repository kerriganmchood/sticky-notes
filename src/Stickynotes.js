// import React, { useState, useRef, useEffect } from 'react';
import React, { useState } from 'react';
import styled from 'styled-components';
import { v4 as uuid } from 'uuid';
// import ContentEditable from 'react-contenteditable';

// Stickynote Style
const Note = styled.div.attrs((props) => (
	{
		// Making Changeable Properties
		style: {
			left: `${props.left}px`,
			top: `${props.down}px`
		}
	}
))`	
	position: absolute;
    width: 14rem;
    height: 14rem;
    color: black;
    text-align: ${(props) => props.align};
	font-size: 1.1em;
    background-color: ${(props) => props.color};
	margin-top: 9rem;
	margin-left: rem;
	box-shadow: 3px 3px 4px black;

`;

// Background Style
const Background = styled.div`
    border: 1px solid #000;
    background-image: url("/images/corkboard.jpeg");
    width: 1680px;
    height: 1750px;
`;

// Header Style
const Header = styled.div`
    background-color: pink;
    width: 105rem;
    height: 6rem;
`;

// Button Style
const PictureButton = styled.img`
	background-image: url(${(props) => props.image});
    position: absolute;
    right: ${(props) => props.right};
	top: ${(props) => props.top};
`;

const Button = styled.button`
 	font-size: 1em;
 	display: inline-block;
 	background-color: ${(props) => props.color};
 	flex-direction: row;
 	border-radius: 12em;
	width: 25px;
	height: 25px;
 	text-align: up;
	border: 0px;
	display: inline-block;
`;

const ColorButtons = styled.button`
	background-color: ${(props) => props.color};
	font-size: 1em;
 	display: inline-block;
 	flex-direction: row;
 	border-radius: 12em;
	width: 25px;
	height: 25px;
	border: 1px solid grey;
	display: inline-block;
	margin-top: 5px;
`;

// Textbox Style
const TextArea = styled.textarea`
	width: 13rem;
	height: 12rem;
	background-color: rgba(0, 0, 0, 0);
	border-color: rgba(0, 0, 0, 0);
`;

export default function Default() {
	// Array Of Notes
	const [notes, setNotes] = useState([]);
	const [showColor, setShowColor] = useState(false);

	// New Note
	const notePush = () => {
		const temporaryNotes = [...notes];
		const noteContent = '';
		const newNote = {
			id: uuid(), text: noteContent, color: 'mistyrose', x: 0, y: 0
		};

		// Adjust Coordinates Of New Note To Prevent Overlap
		newNote.x = 250 * notes.length;

		temporaryNotes.push(newNote);

		setNotes(temporaryNotes);
	};

	// Delete Note
	const noteRemove = (index) => {
		const temporaryNotes = [...notes];
		temporaryNotes.splice(index, 1);
		setNotes(temporaryNotes);
	};

	// Delete All
	const deleteAll = () => {
		const temporaryNotes = [];
		setNotes(temporaryNotes);
	};

	// Get Coordinates For DragnDrop
	const getCoords = (e, index) => {
		// Change Position X&Y Direction
		console.log('e', e);
		const tempArr = [...notes];
		tempArr[index].x = e.screenX - 128;
		tempArr[index].y = e.screenY - 448;
		setNotes(tempArr);
	};

	// On Click Change Color
	const changeColor = (index, newColor) => {
		const tempArr = [...notes];
		tempArr[index].color = newColor;
		setNotes(tempArr);
	};

	// Randomily Change All Colors
	const randomizeColors = () => {
		const tempArr = [...notes];
		const min = 1;
		const max = 6;
		let color = 'blue';
		let i;
		for (i = 0; i < tempArr.length; i++) {
			const rand = min + Math.floor(Math.random() * (max - min));
			if (rand === 1) {
				color = 'mistyrose';
			} else if (rand === 2) {
				color = 'peachpuff';
			} else if (rand === 3) {
				color = 'papayawhip';
			} else if (rand === 4) {
				color = 'honeydew';
			} else if (rand === 5) {
				color = 'aliceblue';
			} else if (rand === 6) {
				color = 'lavender';
			}
			tempArr[i].color = color;
			console.log('rand', rand);
		}
		setNotes(tempArr);
	};

	// Final Display
	return (
		<Background>
			<Header>
				<h1>Sticky Notes</h1>
			</Header>
			{notes.map((note, index) => (
				<Note
					draggable
					onDragEnd={(e) => getCoords(e, index)}
					left={note.x}
					down={note.y}
					color={note.color}
				>
					<div
						onMouseEnter={() => {
							setShowColor(true);
						}}
						onMouseLeave={() => {
							setShowColor(false);
						}}
					>
						<Button color="rgba(0, 0, 0, 0)" onClick={() => noteRemove(index)}>x</Button>
						{!showColor && (
							<Button color="rgba(0, 0, 0, 0)">⋮</Button>
						)}
						{showColor && (
							<>
								<ColorButtons
									color="mistyrose"
									onClick={() => {
										const newColor = 'mistyrose';
										changeColor(index, newColor);
									}}
								/>
								<ColorButtons
									color="peachpuff"
									onClick={() => {
										const newColor = 'peachpuff';
										changeColor(index, newColor);
									}}
								/>
								<ColorButtons
									color="papayawhip"
									onClick={() => {
										const newColor = 'papayawhip';
										changeColor(index, newColor);
									}}
								/>
								<ColorButtons
									color="honeydew"
									onClick={() => {
										const newColor = 'honeydew';
										changeColor(index, newColor);
									}}
								/>
								<ColorButtons
									color="aliceblue"
									onClick={() => {
										const newColor = 'aliceblue';
										changeColor(index, newColor);
									}}
								/>
								<ColorButtons
									color="lavender"
									onClick={() => {
										const newColor = 'lavender';
										changeColor(index, newColor);
									}}
								/>
							</>
						)}
					</div>
					<TextArea align="center">{note.text}</TextArea>
				</Note>
			))}
			<PictureButton src="/images/realtrash.png" right="10px" top="30px" width="80px" height="80px" onClick={() => deleteAll()} />
			<PictureButton src="/images/pinkSticky.jpeg" right="120px" top="30px" width="80px" height="80px" onClick={() => notePush()} />
			<PictureButton src="/images/pastelwheel.jpeg" right="230px" top="30px" width="80px" height="80px" onClick={() => randomizeColors()} />
		</Background>
	);
}
